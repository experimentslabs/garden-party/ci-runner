FROM ruby:3.1-slim-bookworm

ENV NODE_VERSION=20 \
    POSTGRES_VERSION=15 \
    BUNDLER_VERSION=2.4

# - Installs basic tools to build dependencies
# - Adds Node source repository
# - Add Gitlab's "release-cli" for releases
# - Install tools to run tests and libraries
# - Enable corepack to have Yarn
# - Clean APT cache
# - Install SVGo
# - Install the right bundler version
# - Database: trust any user from localhost
RUN bash -c "set -o pipefail \
    && apt-get update > /dev/null \
    && apt-get install --assume-yes --no-install-recommends \
      gpg curl git gcc g++ make \
      > /dev/null \
    && mkdir -p /etc/apt/keyrings/ \
    && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
    && echo 'deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_VERSION}.x nodistro main' | tee /etc/apt/sources.list.d/nodesource.list \
    && apt-get update > /dev/null \
    && apt-get install --assume-yes --no-install-recommends \
      postgresql-${POSTGRES_VERSION} \
      libpq-dev \
      nodejs \
      firefox-esr \
      libvips42 \
      imagemagick \
      > /dev/null \
    && corepack enable \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && apt-get clean \
    && yarn global add --silent \
      svgo@1.3.2 \
    && gem install bundler -v \"~> ${BUNDLER_VERSION}\" \
    && curl --location --output /usr/local/bin/release-cli 'https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/release-cli-linux-amd64' \
    && chmod +x /usr/local/bin/release-cli \
    && echo 'host   all   postgres   localhost   trust' > /etc/postgresql/${POSTGRES_VERSION}/main/pg_hba.conf"

# CI-runner

Base image to run CI jobs for Garden Party.

When used in Gitlab CI, PostgreSQL _service_ is used as a separate image.

To use this image in another CI runner, you can start the included PostgreSQL
server and use the `postgres` user with any password.

```
systemctl start postgresql
```

and, for the database configuration file:

```sh
cat > config/database.yml <<-EOF
---
test:
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  host: localhost
  port: 5432
  database: postgresql
  username: postgres
  password: postgres
EOF
```
